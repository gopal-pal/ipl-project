package com.mountblue;
import java.io.*;
import java.util.*;
public class Main {
    private static ArrayList<Match> matchesData = new ArrayList<>();
    private static ArrayList<Delivery> deliveriesData = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        convertMatchesDataCsvToList();
        convertDeliveriesDataCsvToList();
        findPlayedMatchesPerYear(matchesData);
        findWonMatchesAllTeamAllYear(matchesData);
        findExtraRunsPerTeamsIn2016(matchesData, deliveriesData);
        findTopEconomicalIn2015(matchesData, deliveriesData);
        findMostSixesByPlayer(deliveriesData);
        findTotalWideByBowler(deliveriesData);
        findNumberOfCskVsMiMatches(matchesData);
        findTotalRunOfEachPlayerOverYears(deliveriesData);
        findTotalMatchesInCity(matchesData);
        findBoundariesByRohitSharma(deliveriesData);
    }

    public static void convertMatchesDataCsvToList () throws IOException {
        String matches = "matches.csv";
        String line = "";
        BufferedReader matchesReader = new BufferedReader(new FileReader(matches));
        matchesReader.readLine();
        while ((line = matchesReader.readLine()) != null) {
            Match match = new Match();
            String[] matchData = line.split(",");
            match.setMatchId(matchData[0]);
            match.setSeason(matchData[1]);
            match.setCity(matchData[2]);
            match.setWinner(matchData[10]);
            match.setTeam1(matchData[4]);
            match.setTeam2(matchData[5]);
            matchesData.add(match);
        }
        matchesReader.close();
    }

    public static void convertDeliveriesDataCsvToList () throws IOException {
        String line = "";
        String delivery = "deliveries.csv";
        BufferedReader deliveryReader = new BufferedReader(new FileReader(delivery));
        deliveryReader.readLine();
        while ((line = deliveryReader.readLine()) != null) {
            Delivery deliveries = new Delivery();
            String[] deliveryData = line.split(",");
            deliveries.setMatchId(deliveryData[0]);
            deliveries.setBowlingTeam(deliveryData[3]);
            deliveries.setBall(deliveryData[5]);
            deliveries.setBatsman(deliveryData[6]);
            deliveries.setBowler(deliveryData[8]);
            deliveries.setWideRun(deliveryData[10]);
            deliveries.setExtraRun(deliveryData[16]);
            deliveries.setBatsmanRun(deliveryData[15]);
            deliveries.setTotalRun(deliveryData[17]);
            deliveriesData.add(deliveries);
        }
        deliveryReader.close();
    }

    public static void findPlayedMatchesPerYear(ArrayList<Match> matchesData) {
        Map<String, Integer> matchesPerYear = new HashMap<>();
        for (Match data: matchesData) {
            if (matchesPerYear.containsKey(data.getSeason())) {
                matchesPerYear.put(data.getSeason(), matchesPerYear.get(data.getSeason())+1);
            } else {
                matchesPerYear.put(data.getSeason(), 1);
            }
        }
        System.out.println("1. Number of matches played per year of all the years in IPL.");
        System.out.println(matchesPerYear);
    }

    public static void findWonMatchesAllTeamAllYear (ArrayList<Match> matchesData) {
           Map<String, Integer> wonMatchByAllTeam = new HashMap<>();
           for (Match data : matchesData) {
               if (data.getWinner().equals("")) {
                   continue;
               }
               if (wonMatchByAllTeam.containsKey(data.getWinner())) {
                   wonMatchByAllTeam.put(data.getWinner(), (wonMatchByAllTeam.get(data.getWinner())+1));
               } else {
                   wonMatchByAllTeam.put(data.getWinner(), 1);
               }
           }
           System.out.println("2. Number of matches won of all teams over all the years of IPL.");
           System.out.println(wonMatchByAllTeam);
    }

    public static void findExtraRunsPerTeamsIn2016(ArrayList<Match> matchesData, ArrayList<Delivery> deliveriesData) {
        Map<String, Integer> extraRunPerTeam = new HashMap<>();
        ArrayList<String> yearData = new ArrayList<>();
        for (Match data : matchesData) {
            if (data.getSeason().equals("2016")) {
                yearData.add(data.getMatchId());
            }
        }
        for (Delivery data : deliveriesData) {
                if (yearData.contains(data.getMatchId())) {
                    if (extraRunPerTeam.containsKey(data.getBowlingTeam())) {
                        extraRunPerTeam.put(data.getBowlingTeam(), extraRunPerTeam.get(data.getBowlingTeam()) +
                                Integer.parseInt(data.getExtraRun()));
                    } else {
                        extraRunPerTeam.put(data.getBowlingTeam(), Integer.parseInt(data.getExtraRun()));
                    }
                }
            }
        System.out.println("3. For the year 2016 get the extra runs conceded per team.");
        System.out.println(extraRunPerTeam);
    }

    public static void findTopEconomicalIn2015 (ArrayList<Match> matchesData, ArrayList<Delivery> deliveriesData) {
        ArrayList<String> matchId = new ArrayList<>();
        ArrayList<Delivery> deliveryDataWithId = new ArrayList<>();
        Map<String, List<Integer>> bowlerDetail = new HashMap<>();
        HashMap<String, Float> topEconomicalBowler = new HashMap<>();

        for (Match data : matchesData) {
            if (data.getSeason().equals("2015")) {
                matchId.add(data.getMatchId());
            }
        }
        for (Delivery data : deliveriesData) {
            if (matchId.contains(data.getMatchId())) {
                deliveryDataWithId.add(data);
            }
        }
        for (Delivery data : deliveryDataWithId) {
            if (bowlerDetail.containsKey(data.getBowler())) {
                bowlerDetail.get(data.getBowler()).set(0, bowlerDetail.get(data.getBowler()).get(0) +
                        Integer.parseInt(data.getTotalRun()));
                bowlerDetail.get(data.getBowler()).set(1, bowlerDetail.get(data.getBowler()).get(1) + 1);
            } else {
                bowlerDetail.put(data.getBowler(), new ArrayList<>());
                bowlerDetail.get(data.getBowler()).add(Integer.parseInt(data.getTotalRun()));
                bowlerDetail.get(data.getBowler()).add(1);
            }
        }
        for (Map.Entry<String, List<Integer>> entry : bowlerDetail.entrySet()) {
            List<Integer> value = entry.getValue();
            String bowlerName = entry.getKey();
            float economy = value.get(0) / (value.get(1) / 6);
            topEconomicalBowler.put(bowlerName, economy);
        }
        System.out.println("4. For the year 2015 get the top economical bowlers.");
        HashMap<String,Float> sortedTopEconomicalBowler = sortByValue(topEconomicalBowler);
        System.out.println(sortedTopEconomicalBowler);
    }

    private static HashMap<String, Float> sortByValue(HashMap<String, Float> hm) {
        List<Map.Entry<String, Float> > list = new LinkedList<>(hm.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Float> >() {
            public int compare(Map.Entry<String, Float> o1, Map.Entry<String, Float> o2)
            {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        HashMap<String, Float> result = new LinkedHashMap<>();
        for (Map.Entry<String, Float> data : list) {
            result.put(data.getKey(), data.getValue());
        }
        return result;
    }

    public static void findMostSixesByPlayer (ArrayList<Delivery> deliveriesData) {
        Map<String, Integer> mostSixes = new HashMap<>();
        for (Delivery data : deliveriesData) {
            if (data.getBatsmanRun().equals("6")) {
                if (mostSixes.containsKey(data.getBatsman())) {
                    mostSixes.put(data.getBatsman(), mostSixes.get(data.getBatsman())+1);
                } else {
                    mostSixes.put(data.getBatsman(), 1);
                }
            }
        }
        System.out.println("5. Get Most Sixes by player over all years");
        System.out.println(mostSixes);
    }

    public static void findTotalWideByBowler (ArrayList<Delivery> deliveriesData) {
        Map<String, Integer> wideBall = new HashMap<>();
        for (Delivery data : deliveriesData) {
            if (!data.getWideRun().equals("0")) {
                if (wideBall.containsKey(data.getBowler())) {
                    wideBall.put(data.getBowler(), wideBall.get(data.getBowler())+1);
                } else {
                    wideBall.put(data.getBowler(), 1);
                }
            }
        }
        System.out.println("6. Get Total wide of each bowler over all years");
        System.out.println(wideBall);
    }

    public static void findNumberOfCskVsMiMatches (ArrayList<Match> matchesData) {
        int cskVsMi = 0;
        for (Match data : matchesData) {
            if ((data.getTeam1().equals("Mumbai Indians") & data.getTeam2().equals("Chennai Super Kings")) |
                    (data.getTeam1().equals("Chennai Super Kings") & data.getTeam2().equals("Mumbai Indians"))) {
                cskVsMi++;
            }
        }
        System.out.println("7. Total Matches of CSK vs MI : "+cskVsMi);
    }

    public static void findTotalRunOfEachPlayerOverYears (ArrayList<Delivery> deliveriesData) {
        Map<String, Integer> runData = new HashMap<>();
        for (Delivery data : deliveriesData) {
            if (runData.containsKey(data.getBatsman())) {
                runData.put(data.getBatsman(), (runData.get(data.getBatsman())) +
                        Integer.parseInt(data.getBatsmanRun()));
            } else {
                runData.put(data.getBatsman(), Integer.parseInt(data.getBatsmanRun()));
            }
        }
        System.out.println("8. Get Total Catch of players over years.");
        System.out.println(runData);
    }

    public static void findTotalMatchesInCity (ArrayList<Match> matchesData) {
        Map<String, Integer> matchCity = new HashMap<>();
        for (Match data : matchesData) {
            if (matchCity.containsKey(data.getCity())) {
                matchCity.put(data.getCity(), (matchCity.get(data.getCity()))+1);
            } else {
                matchCity.put(data.getCity(), 1);
            }
        }
        System.out.println("9. Get Total matches in a every City");
        System.out.println(matchCity);
    }

    public static void findBoundariesByRohitSharma (ArrayList<Delivery> deliveriesData) {
        int runsByRohit = 0;
        for (Delivery data : deliveriesData) {
            if (data.getBatsman().equals("RG Sharma") & (data.getBatsmanRun().equals("4") |
                    data.getBatsman().equals("6"))) {
                runsByRohit++;
            }
        }
        System.out.println("10. Total Boundaries By Rohit Sharma over all years : " + runsByRohit);
    }
}