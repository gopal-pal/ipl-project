package com.mountblue;

public class Delivery {
    private String matchId;
    private String ball;
    private String batsman;
    private String bowler;
    private String wideRun;
    private String batsmanRun;
    private String extraRun;
    private String totalRun;
    private String bowlingTeam;

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getBall() {
        return ball;
    }

    public void setBall(String ball) {
        this.ball = ball;
    }

    public String getBatsman() {
        return batsman;
    }

    public void setBatsman(String batsman) {
        this.batsman = batsman;
    }

    public String getBowler() {
        return bowler;
    }

    public void setBowler(String bowler) {
        this.bowler = bowler;
    }

    public String getWideRun() {
        return wideRun;
    }

    public void setWideRun(String wideRun) {
        this.wideRun = wideRun;
    }

    public String getBatsmanRun() {
        return batsmanRun;
    }

    public void setBatsmanRun(String batsmanRun) {
        this.batsmanRun = batsmanRun;
    }

    public String getExtraRun() {
        return extraRun;
    }

    public void setExtraRun(String extraRun) {
        this.extraRun = extraRun;
    }

    public String getTotalRun() {
        return totalRun;
    }

    public void setTotalRun(String totalRun) {
        this.totalRun = totalRun;
    }

    public String getBowlingTeam() {
        return bowlingTeam;
    }

    public void setBowlingTeam(String bowlingTeam) {
        this.bowlingTeam = bowlingTeam;
    }

}